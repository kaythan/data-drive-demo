# DataDrive Dashboard Demo

This project is a demo web portal with an embedded Tableau analytics dashboard. It's written with [Vue.js](https://vuejs.org). As of now there is no server-side application. Everything is ran in the browser.

The project was initially generated with the [Vue CLI](https://cli.vuejs.org). In addition to the vanilla generation, a few extra dependencies/features have been added: [Sass](https://sass-lang.com), [Typescript](https://typescriptlang.org) support, and [Tailwind CSS](https://tailwindcss.com).

## Development Environment Setup

`nodejs` and `npm` are required. [Prettier](https://prettier.io) is recommended for keeping source code stylistically consistent.

## Build

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
