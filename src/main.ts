import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { Auth0 } from "@/auth";
import "./index.scss";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faFilter,
  faFileDownload,
  faUndoAlt,
  faChevronDown,
  faChevronRight,
  faQuestionCircle,
  faCheck,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faFilter, faFileDownload, faUndoAlt, faChevronDown, faChevronRight, faQuestionCircle, faCheck);

async function init() {
  const AuthPlugin = await Auth0.init({
    onRedirectCallback: _appState => {
      setTimeout(() => {
        router.push({ name: "portal" });
      }, 50);
    },
    clientId: process.env.VUE_APP_AUTH0_CLIENT_KEY,
    domain: process.env.VUE_APP_AUTH0_DOMAIN,
    audience: "",
    redirectUri: `${window.location.origin}${process.env.NODE_ENV === "production" ? "/data-drive-demo/" : ""}`,
  });
  const app = createApp(App);

  app.directive("click-outside", {
    beforeMount(el, binding, _vnode) {
      el.clickOutsideEvent = function(event: any) {
        if (!(el === event.target || el.contains(event.target))) {
          binding.value(event, el);
        }
      };
      document.body.addEventListener("click", el.clickOutsideEvent);
    },
    unmounted(el) {
      document.body.removeEventListener("click", el.clickOutsideEvent);
    },
  });

  app
    .use(AuthPlugin)
    .use(router)
    .component("font-awesome-icon", FontAwesomeIcon)
    .mount("#app");
}

init();
