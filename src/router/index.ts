import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Landing from "../pages/Landing.vue";
import Portal from "../pages/Portal.vue";
import { Auth0 } from "@/auth";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "landing",
    component: Landing,
  },
  {
    path: "/portal",
    name: "portal",
    component: Portal,
    beforeEnter: Auth0.routeGuard,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
