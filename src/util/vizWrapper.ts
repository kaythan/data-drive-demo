export default class VizWrapper {
  private viz: any;
  private workbook: any;
  private activeSheet: any;
  private tableau: any;

  constructor(viz: any, tableau: any) {
    this.viz = viz;
    this.workbook = viz.getWorkbook();
    this.activeSheet = this.workbook.getActiveSheet();
    this.tableau = tableau;
  }

  // async getFilters(): Promise<Filter[]> {
  //   const filters = await this.activeSheet.getFiltersAsync();
  //   debugger;
  //   return filters.map((f: any) => ({
  //     name: f.getFieldName(),
  //     options: f.getAppliedValues(),
  //     type: f.getFilterType(),
  //   }));
  // }

  // async replaceFilter(filterId: String, optionId: String) {
  //   await this.activeSheet.applyFilterAsync(filterId, optionId, this.tableau.FilterUpdateType.REPLACE);
  // }

  async replaceFilter(filterId: String, optionIds: String[]) {
    await this.activeSheet.applyFilterAsync(filterId, optionIds, this.tableau.FilterUpdateType.REPLACE);
  }

  exportToPdf() {
    this.viz.showExportPDFDialog();
  }

  exportToCsv() {
    this.viz.showExportDataDialog();
  }

  exportToCrosstab() {
    this.viz.showExportCrossTabDialog();
  }
}

export const FILTER_TYPES = {
  CATEGORICAL: "categorical",
};

// export interface FilterOption {
//   value: String;
//   formattedValue: String;
// }
//
// export interface Filter {
//   name: String;
//   options: FilterOption[];
//   type: String;
// }

export interface FilterOptionConfig {
  text: String;
  id: String;
}

export interface FilterConfig {
  icon?: String;
  name: String;
  id: String;
  helpText: String | null;
  type: "categorical";
  subType: null | "single" | "multi";
  options: FilterOptionConfig[];
}
