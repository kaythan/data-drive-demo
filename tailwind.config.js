const colors = require("tailwindcss/colors");

module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    colors: {
      transparent: "transparent",
      current: "currentColor",
      cyan: colors.cyan,
      gray: colors.warmGray,
      teal: colors.teal,
      black: colors.black,
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
