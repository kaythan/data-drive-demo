module.exports = {
  // publicPath: "/data-drive-demo/",
  publicPath: process.env.NODE_ENV === "production" ? "/data-drive-demo/" : "/",
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "@/_variables.scss";`,
      },
    },
  },
};
